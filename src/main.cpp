#include "raiseApp.h"
#include "ofMain.h"

/*! \mainpage rAIse
 *
 * \section intro_sec Introduction
 * This repo contains the source code of an ongoing art project which involves motion capture and kinethics analysis in a dance show about slowliness and inner speed.
 */

int main() {
  ofSetupOpenGL(1400, 900, OF_WINDOW);
  ofRunApp(new RaiseApp()); 
}