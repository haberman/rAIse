#include "raiseApp.h"

#include "mmGeometry.h"    //enables some geometry techniques (such as translation)
#include "mmKinematics.h"  //enables some kinematics techniques (such as speed computation)
#include "mmSignal.h"      //enables some signal processing techniques (such as filtering)

#include "raise/utils.h"

// using namespace rAIse;

void RaiseApp::setup(void) {
  ofSetFrameRate(30);
  // ofSetBackgroundAuto(false);
  // ofEnableAlphaBlending();

  Track track;
  track.load(getAbsoluteResPath() + "taichi.v3d");
  track.setFrameRate(179);  // Qualisys file
  track = meanfilter(track, 5);

  track_ = make_shared<Track>(track);
  active_modes_.emplace_back(rAIse::MODE_RANDOM);

  ofLogNotice() << "track " << track_->fileName
                << " has " << track_->nodeList->size() << " nodes"
                << " and " << track_->boneList->size() << " bones.";

  setFrameRate(track_->frameRate());
  setPlayerSize(track_->nOfFrames());

  showFigures(false);
  showShortcuts(false);
  showGround(false);

  setup_panels();

  skeleton_ = new rAIse::Skeleton(this, track_, active_modes_.at(0));

  ambient_light_.setAreaLight(1000.f, 1000.f);
  ambient_light_.setPosition({0.f, 0.f, 2000.f});

  point_light_.setPointLight();
  point_light_.setDiffuseColor(ofColor::blue);

  time_setup.getFloat("phase").addListener(this, &RaiseApp::timephase_changed);
  time_setup.getFloat("shift").addListener(this, &RaiseApp::timeshift_changed);

  trail_setup.getInt("amount").addListener(this, &RaiseApp::trailamount_changed);
  trail_setup.getInt("jump").addListener(this, &RaiseApp::trailjump_changed);
}

void RaiseApp::setup_panels() {
  drawing_panel = new rAIse::DrawingPanel(this);

  vector<ofJson> fbody_style;
  fbody_style.emplace_back(ofJson({{"width", "50%"}}));
  fbody_style.emplace_back(ofJson({{"width", "50%"}}));

  ofParameterGroup fbody_params;
  fbody_params.add(body_bounce.set("bounce", false));
  fbody_params.add(body_bounces.set("amount", 1u, 1u, 10u));

  body_panel = new rAIse::ModePanel(this,
                                    rAIse::MODE_BODY,
                                    fbody_params,
                                    fbody_style,
                                    drawing_panel,
                                    250u);

  body_panel->set_active(true);

  vector<ofJson> random_styles;
  random_styles.emplace_back(ofJson({{"width", "50%"}, {"precision", 3}}));  // mix
  random_styles.emplace_back(ofJson({{"width", "50%"}}));                    // amount

  ofParameterGroup random_params;
  random_params.add(random_mix.set("mix", .5f));
  random_params.add(random_amount.set("amount", 10u, 1u, 21u));

  random_panel = new rAIse::ModePanel(this,
                                      rAIse::MODE_RANDOM,
                                      random_params,
                                      random_styles,
                                      body_panel,
                                      250u);

  const ofJson rotary_style = {{"width", "50%"},
                               {"height", 85},
                               {"margin", "10 4 5 4"},
                               {"type", "circular"},
                               {"precision", 2}};
  vector<ofJson> pproj_style;
  pproj_style.emplace_back(rotary_style);                                  // zenith
  pproj_style.emplace_back(rotary_style);                                  // azimuth
  pproj_style.emplace_back(ofJson({{"width", "50%"}}));                    // distance
  pproj_style.emplace_back(ofJson({{"width", "50%"}, {"precision", 3}}));  // deviance

  ofParameterGroup pproj_params;
  pproj_params.add(project_zenith.set("zenith", 0.f, -M_PI, M_PI));
  pproj_params.add(project_azimuth.set("azimuth", 0.f, -M_PI, M_PI));
  pproj_params.add(project_distance.set("distance", 0, rAIse::MIN_DISTANCE, rAIse::MAX_DISTANCE));
  pproj_params.add(project_deviance.set("deviance", 0.f, 0.f, 1.f));

  project_panel = new rAIse::ModePanel(this,
                                       rAIse::MODE_PROJECT,
                                       pproj_params,
                                       pproj_style,
                                       random_panel,
                                       250u);
}

void RaiseApp::timephase_changed(float& value) {
  skeleton_->apply_timephase(value);
}

void RaiseApp::timeshift_changed(float& value) {
  skeleton_->apply_timeshift(value);
}

void RaiseApp::trailamount_changed(int& value) {
  skeleton_->apply_trailamount(value);
}

void RaiseApp::trailjump_changed(int& value) {
  skeleton_->apply_trailjump(value);
}

void RaiseApp::update(void) {
  if (drawing_panel->getContainer()->isMousePressed()) {
    camera.disableMouseInput();
  } else {
    camera.enableMouseInput();
  }

  frame_ = track_->frame(getAppTime());
}

void RaiseApp::scene3d(void) {
  const unsigned now = getAppIndex();
  ofBackground(0.f, 1.f);

  // draw(frame_);
  // ofEnableLighting();
  // ambient_light_.enable();

  // if (-1 + randu() * 2 > sin(now)) {
  //   const glm::vec3 pos = Utils::sphere_projection({0.f, 0.f, 1000.f}, 2000.f);
  //   point_light_.enable();
  //   point_light_.setPosition(pos);
  // } else if (point_light_.getIsEnabled()) {
  //   point_light_.disable();
  // }

  if (body_panel->active()) {
    skeleton_->draw_body(&frame_,
                         now,
                         body_panel->parameters(),
                         time_setup,
                         line_setup,
                         trail_setup);
  } else {
    skeleton_->clean_mode(rAIse::MODE_BODY);
  }

  if (random_panel->active()) {
    skeleton_->draw_random(&frame_,
                           now,
                           random_panel->parameters(),
                           time_setup,
                           line_setup,
                           trail_setup);
  } else {
    skeleton_->clean_mode(rAIse::MODE_RANDOM);
  }

  if (project_panel->active()) {
    skeleton_->draw_projection(&frame_,
                               now,
                               project_panel->parameters(),
                               time_setup,
                               line_setup,
                               trail_setup);
  } else {
    skeleton_->clean_mode(rAIse::MODE_PROJECT);
  }

  // ambient_light_.disable();
  // ofDisableLighting();

  // @TODO -> decide what to when implementing camera
  // if (!isNodeNames) return;

  // for (auto node : skeleton_->active_nodes(active_modes)) {
  //   const string& tag = frame_.nodeList->name(node);

  //   ofSetColor(ofColor::lightPink);
  //   ofDrawBitmapString(tag, toVec3f(frame_.getPosition().col(node)) + nodeSize / 1.5f);
  // }
}

void RaiseApp::scene2d(void) {
}

void RaiseApp::annotate(void) {
}

void RaiseApp::keyPressed(int key) {
}

void RaiseApp::keyReleased(int key) {
}

void RaiseApp::mousePressed(int x, int y, int button) {
}

void RaiseApp::mouseReleased(int x, int y, int button) {
}

void RaiseApp::mouseDragged(int x, int y, int button) {
}

void RaiseApp::mouseMoved(int x, int y) {
}

void RaiseApp::windowResized(int w, int h) {
}

void RaiseApp::dragEvent(ofDragInfo dragInfo) {
}

void RaiseApp::gotMessage(ofMessage msg) {
}

void RaiseApp::exit() {
  ofLogNotice() << "cleaning up";

  delete project_panel;
  delete random_panel;
  delete body_panel;
  delete drawing_panel;

  delete skeleton_;

  ofLogNotice() << "I'm out";
}
