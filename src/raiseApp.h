#pragma once

#include <armadillo>

#include "raise/drawingPanel.h"
#include "raise/modePanel.h"
#include "raise/skeleton.h"

#include "MoMa.h"
#include "MoMaUI.h"
#include "ofMain.h"

using namespace MoMa;
using namespace std;

/** @brief Main application. */
class RaiseApp : public SceneApp {
 public:
  RaiseApp() {}
  /** @brief Callback function that will creates the drawing panel. */
  void setup(void);

  /** @brief Callback function called when user change trails amount. */
  void trailamount_changed(int&);

  /** @brief Callback function called when user change trailing jumps value. */
  void trailjump_changed(int&);

  /** @brief Callback function called when user change time phase. */
  void timephase_changed(float&);

  /** @brief Callback function called when user change time shift. */
  void timeshift_changed(float&);

  /** @brief Callback function called when user change projection. */
  void projection_changed(float&);

  void update(void);
  void scene3d(void);
  void scene2d(void);
  void annotate(void);

  void keyPressed(int key);
  void keyReleased(int key);

  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void mouseDragged(int x, int y, int button);
  void mouseMoved(int x, int y);

  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);
  void exit();

  rAIse::DrawingPanel* drawing_panel;

  rAIse::ModePanel* random_panel;
  rAIse::ModePanel* project_panel;
  rAIse::ModePanel* body_panel;

  ofParameterGroup line_setup;
  ofParameterGroup time_setup;
  ofParameterGroup trail_setup;

  // Mode specific options
  ofParameter<bool> body_bounce;
  ofParameter<int> body_bounces;

  ofParameter<float> random_mix;
  ofParameter<int> random_amount;

  ofParameter<float> project_zenith;
  ofParameter<float> project_azimuth;
  ofParameter<int> project_distance;
  ofParameter<float> project_deviance;

 protected:
  /** @brief Create drawing modes panels. */
  void setup_panels();

  shared_ptr<Track> track_;
  Frame frame_;
  vector<string> active_modes_;

  rAIse::Skeleton* skeleton_;
  vector<string> trailing_nodes_{"Head", "Thorax"};

  ofLight ambient_light_, point_light_;
};