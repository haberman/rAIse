#include "modePanel.h"

rAIse::ModePanel::ModePanel(SceneApp* app,
                            const string& title,
                            ofParameterGroup parameters,
                            const vector<ofJson> styles,
                            Canvas* parent_ui,
                            const unsigned width,
                            const Position position,
                            const Position alignment) : Canvas(app,
                                                               title,
                                                               Canvas::Type::Group,
                                                               position,
                                                               alignment,
                                                               parent_ui),
                                                        parameters_(parameters) {
  const ofJson flex_style = {{"width", width},
                             {"flex-direction", "row"},
                             {"flex-wrap", "wrap"},
                             {"margin", "2 2 10 2"}};

  _container->setConfig(flex_style);
  _container->add(active_.set("active", false), {{"width", "100%"}});

  if (parameters_.size() != styles.size()) {
    ofLogWarning() << "Ignoring because parameters size doesn't match styles' one.";
    return;
  }

  for (unsigned short i = 0; i < styles.size(); ++i) {
    const string T = parameters_.get(i).valueType();
    const string N = parameters_.get(i).getName();
    const ofJson& item_style = styles.at(i);

    if (T == "f") {
      _container->add(parameters_.getFloat(N), item_style);
    } else if (T == "i") {
      _container->add(parameters_.getInt(N), item_style);
    } else if (T == "b") {
      _container->add(parameters_.getBool(N), item_style);
    }
  }

  initCanvas();
}