#pragma once

#include <armadillo>
#include "ofMain.h"

using namespace std;

namespace rAIse {

/** @brief Describes a drawing segment. **/
struct Segment {
  float progress = 0.f;
  float twist = 0.f;
  float phase = 0.f;
  float shift = 0.f;

  vector<glm::vec3> points;
  glm::quat rotation;
};

/**
 * @brief Traversable object that represents a bone inside a skeleton.
 * 
 * Meaning that it's actually made of two points: a child and a parent that could be recursively requested to traverse skeleton at the point.
 * Internally uses a rAIse::Segment instance that describes individual behavior to adopt when drawing.
 */
class Node {
 public:
  /**
   * @brief Construct a new Node object.
   * 
   * @param id A unique node id
   */
  Node(const unsigned id = 0) : id_(id) {
    Segment init_segment;
    segments_.emplace_back(init_segment);
  }

  /** @brief Destroy the Node object. */
  ~Node() {
    if (parent_) parent_.reset();
    if (child_) child_.reset();
  }

  /** @brief Prepare this node to accept no futher points and mark itself as being deletable. */
  void prepare_destroy() { destroyable_ = true; }

  /**
   * @brief Setup the Segment at provided index to be a clone of a given reference.
   * 
   * @param index The index at which segment should be configured
   * @param ref A reference to a source 
   */
  void setup_segment(unsigned index, const Segment& ref) {
    if (index >= segments_.size()) return;

    Segment& segment = segments_.at(index);
    segment.phase = ref.phase;
    segment.rotation = ref.rotation;
    segment.shift = ref.shift;
    segment.twist = ref.twist;
  }

  /**
   * @brief Draws an oriented segment given a quaternion parameter.
   * 
   * @param parent_pos The parent joint position
   * @param node_pos The bone node position
   * @param bone_orientation Pointer to an `glm::quat` representing the node's bone orientation
   * @param scale The distance between parent & node as a mutliplier for an oriented vector
   * @param time_setup Time drawing options
   * @param line_setup Line drawing options
   */
  void draw(const glm::vec3& parent_pos,
            const glm::vec3& node_pos,
            const unique_ptr<glm::quat> bone_orientation,
            const float& scale,
            const ofParameterGroup& time_setup,
            const ofParameterGroup& line_setup);

  /**
   * @brief Draw a simple segment between two nodes of the sekeleton.
   * 
   * @param parent_pos The parent joint position
   * @param node_pos The bone node position
   * @param time_setup Time drawing options
   * @param line_setup Line drawing options
   */
  void draw(const glm::vec3&,
            const glm::vec3&,
            const ofParameterGroup& time_setup,
            const ofParameterGroup& line_setup,
            const bool use_projection = false);

  /** Returns the node id. */
  const int id() const { return id_; }

  /** @brief Return the list of segments currently drawn as trails. */
  vector<Segment>& segments() { return segments_; }

  /** @brief Return `true` if node is ready to be destroyed. */
  const bool destroyable() { return destroyable_ && segments_.size() <= 1; }

  /** @brief Return `true` if top drawing segment progress is within bone. */
  const bool in() { return in_; }

  /** @brief Return a pointer to the parent node. */
  const shared_ptr<Node> parent() { return parent_; }

  /** @brief Set the parent object. */
  void set_parent(shared_ptr<Node> node) { parent_ = node; }

  /** @brief Return a pointer to the child node. */
  const shared_ptr<Node> child() { return child_; }

  /** @brief Set the child object. */
  void set_child(shared_ptr<Node> node) { child_ = node; }

  /** @brief Set the amount of frames to jump between trails. */
  void set_trail_jump(const unsigned value) { trail_jump_ = value; }

  /** @brief Set the amount of trails to draw. */
  void set_trail_amount(const unsigned value) { trail_amount_ = value; }

  /** @brief `<<` Operator to print a brief info of this node. */
  friend ostream& operator<<(ostream& os, shared_ptr<Node> node) {
    os << "Node " << node->id() << " ___\n";

    if (node->parent())
      os << "  - parent: " << node->parent()->id() << "\n";

    if (node->child())
      os << "  - child: " << node->child()->id();

    return os << endl;
  }

  ofParameterGroup tmp_setup;

 private:
  unsigned id_;

  unsigned trail_amount_;
  unsigned trail_jump_;

  shared_ptr<Node> parent_;
  shared_ptr<Node> child_;

  vector<Segment> segments_;

  bool destroyable_ = false;
  bool in_ = false;

  /**
   * @brief Compute a new segment and draw trailings.
   * 
   * @param append Should we append the computed segment in the #segments_ list?
   * @param root The root node position (where drawing starts)
   * @param diff A resolution vector that moves on bone direction
   * @param time_setup An `ofParamaterGroup` for line drawing parameters
   * @param line_setup An `ofParamaterGroup` for line drawing parameters
   * @param bone_rot An optional pointer to a quaternion describing the bone rotation
   */
  void draw_segments(const bool append,
                     const glm::vec3& root,
                     const glm::vec3& diff,
                     const ofParameterGroup& time_setup,
                     const ofParameterGroup& line_setup,
                     const glm::quat* bone_rot = nullptr);
};

}  // namespace rAIse