#include <glm/gtx/euler_angles.hpp>

#include "../raiseApp.h"

#include "skeleton.h"
#include "utils.h"

// STATIC >>>
vector<int> rAIse::Skeleton::query_fw(map<int, shared_ptr<Node>> nodes,
                                      int node_id) {
  vector<int> ret;
  ret.emplace_back(node_id);

  int running_id = node_id;
  do {
    running_id = nodes[running_id]->child()->id();
    ret.emplace_back(running_id);
  } while (nodes[running_id]->child());

  return ret;
}

vector<int> rAIse::Skeleton::query_rw(map<int, shared_ptr<Node>> nodes,
                                      int node_id) {
  vector<int> ret;
  ret.emplace_back(node_id);

  int running_id = node_id;
  do {
    running_id = nodes[running_id]->parent()->id();
    ret.emplace_back(running_id);
  } while (nodes[running_id]->parent());

  return ret;
}

vector<int> rAIse::Skeleton::query_random(map<int, shared_ptr<Node>> nodes,
                                          unsigned amount) {
  vector<int> ret;

  for (unsigned k = 0; k < amount; ++k)
    ret.emplace_back(floor(arma::randu() * nodes.size()));

  return ret;
}
// <<< STATIC

rAIse::Skeleton::Skeleton(SceneApp* app,
                          shared_ptr<Track> track,
                          const string& mode) : app_(app),
                                                track_(track) {
  const RaiseApp* raiseApp = static_cast<RaiseApp*>(app_);

  // skeleton is abstracted into a traversable object to make it more interactive.
  for (auto bone_it = track_->boneList->begin(); bone_it != track_->boneList->end(); ++bone_it) {
    const int bone_id = bone_it->second.boneId;
    const int parent_id = bone_it->second.jointParent;

    if (nodes_[bone_id] == nullptr)
      nodes_[bone_id] = make_shared<Node>(bone_id);
    if (nodes_[parent_id] == nullptr)
      nodes_[parent_id] = make_shared<Node>(parent_id);

    nodes_[bone_id]->set_parent(nodes_[parent_id]);

    const int child_id = bone_it->second.jointChildren[0];
    if (nodes_[child_id] == nullptr)
      nodes_[child_id] = make_shared<Node>(child_id);

    nodes_[bone_id]->set_child(nodes_[child_id]);
  }

  // @TODO -> implements
  joint_style_.color = {ofColor::floralWhite, 10};
  joint_style_.lineWidth = 2.f;

  bone_style_.color = {ofColor::indianRed, 10};
  bone_style_.lineWidth = 2.f;
}

rAIse::Skeleton::~Skeleton() {
  for (auto node : nodes_) node.second.reset();
  track_.reset();
}

void rAIse::Skeleton::apply_timephase(const float& value) {
  for (auto mode : active_nodes_) {
    for (unsigned i = 0u; i < mode.second.size(); ++i) {
      const shared_ptr<Node> node = active_nodes_[mode.first][i];

      node->segments().front().phase = 1.f + arma::randu() * value;
      node->segments().front().progress = 0.f;
    }
  }
}

void rAIse::Skeleton::apply_timeshift(const float& value) {
  for (auto mode : active_nodes_)
    for (unsigned i = 0u; i < mode.second.size(); ++i)
      active_nodes_[mode.first][i]->segments()[0].shift = arma::randu() * M_PI * value;  // @TODO -> that raw 0 looks very unsafe to me
}

void rAIse::Skeleton::apply_trailamount(const int& value) {
  for (auto mode : active_nodes_) {
    for (unsigned i = 0u; i < mode.second.size(); ++i) {
      active_nodes_[mode.first][i]->set_trail_amount(value);
    }
  }
}

void rAIse::Skeleton::apply_trailjump(const int& value) {
  for (auto mode : active_nodes_) {
    for (unsigned i = 0u; i < mode.second.size(); ++i) {
      active_nodes_[mode.first][i]->set_trail_jump(value);
    }
  }
}

void rAIse::Skeleton::draw_body(const Frame* frame,
                                const int now,
                                const ofParameterGroup& body_setup,
                                const ofParameterGroup& time_setup,
                                const ofParameterGroup& line_setup,
                                const ofParameterGroup& trail_setup) {
  const arma::mat& frame_rotation = frame->getRotation();
  const arma::mat& frame_position = frame->getPosition();
  if (!frame_rotation.is_finite() || !frame_position.is_finite()) return;

  vector<shared_ptr<Node>>& active_nodes = active_nodes_[rAIse::MODE_BODY];
  if (active_nodes.size() != nodes_.size())  // we use every available nodes
  {
    copy_nodes(rAIse::MODE_BODY,
               trail_setup.getInt("amount"),
               trail_setup.getInt("jump"));
  }

  for (unsigned i = 0; i < active_nodes.size(); ++i) {
    const auto bone_node = active_nodes[i];

    unsigned child_id;
    if (bone_node->child()) {
      child_id = bone_node->child()->id();
    } else {
      continue;
    }

    unsigned parent_id;
    if (bone_node->parent()) {
      parent_id = bone_node->parent()->id();
    } else {
      continue;
    }

    const arma::colvec bone_rot = frame->getRotation().col(bone_node->id());
    const arma::colvec offset_rot = frame->getRotationOffset().col(child_id);
    const auto parent_pos = frame->getPosition().col(parent_id);
    const auto child_pos = frame->getPosition().col(child_id);

    if (offset_rot.is_finite()) {
      bone_node->draw(toVec3f(parent_pos),
                      toVec3f(child_pos),
                      make_unique<glm::quat>(toQuaternion(offset_rot) * toQuaternion(bone_rot)),
                      arma::norm(parent_pos - child_pos),
                      time_setup,
                      line_setup);
    }
  }
}

void rAIse::Skeleton::draw_random(const Frame* frame,
                                  const int now,
                                  const ofParameterGroup& random_setup,
                                  const ofParameterGroup& time_setup,
                                  const ofParameterGroup& line_setup,
                                  const ofParameterGroup& trail_setup) {
  const arma::mat& frame_rotation = frame->getRotation();
  const arma::mat& frame_position = frame->getPosition();
  if (!frame_rotation.is_finite() || !frame_position.is_finite()) return;

  vector<shared_ptr<Node>>& active_nodes = active_nodes_[MODE_RANDOM];
  const unsigned amount = random_setup.getInt("amount");
  const float mix = random_setup.getInt("mix");

  short k;
  // add as many (unused) nodes to match requested amount
  if (active_nodes.size() < amount) {
    Segment* front_segment = nullptr;

    if (active_nodes.size() > 0) {
      front_segment = &active_nodes.front()->segments().front();
    }

    k = amount - active_nodes.size();
    while (k > 0) {
      shared_ptr<Node> node_rnd = nodes_[rand() % nodes_.size()];
      const auto it = find_if(active_nodes.begin(),
                              active_nodes.end(),
                              [node_rnd](const shared_ptr<Node> n) -> bool { return node_rnd->id() == n->id(); });

      if (it == active_nodes.end()) {
        shared_ptr<Node> new_node = make_shared<Node>(*node_rnd);
        new_node->set_trail_amount(trail_setup.getInt("amount"));
        new_node->set_trail_jump(trail_setup.getInt("jump"));

        if (front_segment) {
          new_node->setup_segment(0, *front_segment);
        }

        active_nodes.emplace_back(new_node);
        --k;
      }
    }
    // destroy one elligible node
  } else {
    const auto it = find_if(active_nodes.begin(),
                            active_nodes.end(),
                            [](const shared_ptr<Node> n) -> bool { return n->destroyable(); });

    if (it != active_nodes.end()) {
      active_nodes.erase(it);
    }
  }

  // mark overflow as destroyables
  k = active_nodes.size() - amount;
  while (k > 0) {
    active_nodes[k]->prepare_destroy();
    --k;
  }

  // inject some turbulence by marking a new destroyable when mix is above threshold
  if (mix * arma::randu() > .5f && active_nodes.size() >= amount) {
    const auto it = find_if(active_nodes.begin(),
                            active_nodes.end(),
                            [](const shared_ptr<Node> n) -> bool { return !n->destroyable(); });

    if (it != active_nodes.end()) {
      it->get()->prepare_destroy();
    }
  }

  if (active_nodes.size() == 0) {
    return;
  }

  // draw nodes
  for (unsigned i = 0; i < active_nodes.size(); ++i) {
    const auto bone_node = active_nodes[i];

    unsigned child_id;
    if (bone_node->child()) {
      child_id = bone_node->child()->id();
    } else {
      continue;
    }

    unsigned parent_id = bone_node->id();
    // const auto it = find_if(active_nodes.begin(),
    //                         active_nodes.end(),
    //                         [](const shared_ptr<Node> n) -> bool { return !n->in(); });
    // if (it != active_nodes.end()) parent_id = it->get()->id();

    const auto parent_pos = frame->getPosition().col(parent_id);
    const auto child_pos = frame->getPosition().col(child_id);

    bone_node->draw(toVec3f(parent_pos), toVec3f(child_pos), time_setup, line_setup);
  }
}

void rAIse::Skeleton::draw_projection(const Frame* frame,
                                      const int now,
                                      const ofParameterGroup& project_setup,
                                      const ofParameterGroup& time_setup,
                                      const ofParameterGroup& line_setup,
                                      const ofParameterGroup& trail_setup) {
  const arma::mat& frame_rotation = frame->getRotation();
  const arma::mat& frame_position = frame->getPosition();

  if (!frame_rotation.is_finite() || !frame_position.is_finite()) {
    return;
  }

  vector<shared_ptr<Node>>& active_nodes = active_nodes_[MODE_PROJECT];

  if (active_nodes.size() != nodes_.size())  // we use every available nodes
  {
    copy_nodes(MODE_PROJECT, trail_setup.getInt("amount"), trail_setup.getInt("jump"));
  }

  if (active_nodes.size() == 0) {
    return;
  }

  float zenith = project_setup.getFloat("zenith");
  float azimuth = project_setup.getFloat("azimuth");

  for (unsigned i = 0; i < active_nodes.size(); ++i) {
    const shared_ptr<Node> active_node = active_nodes[i];

    if (!active_node->in()) {
      zenith += sin(arma::randu() * M_PI * 2) * project_setup.getFloat("deviance") * .1f;
      azimuth += sin(arma::randu() * M_PI * 2) * project_setup.getFloat("deviance") * .1f;

      ofParameter<float> zenith_param, azimuth_param;
      active_node->tmp_setup.clear();
      active_node->tmp_setup.add(zenith_param.set("zenith", zenith));
      active_node->tmp_setup.add(azimuth_param.set("azimuth", azimuth));
    } else {
      zenith = active_node->tmp_setup.getFloat("zenith");
      azimuth = active_node->tmp_setup.getFloat("azimuth");
    }

    const glm::vec3& plane_normal = Utils::project_sphere(glm::vec3(), zenith, azimuth, 1.f);

    const glm::vec3& plane_distance = Utils::project_sphere(glm::vec3(),
                                                            zenith,
                                                            azimuth,
                                                            project_setup.getInt("distance"));

    const glm::vec3& node_pos = toVec3f(frame_position.col(active_node->id()));
    const glm::vec3& proj_pos = Utils::project_plane(node_pos, plane_normal, plane_distance);

    active_node->draw(proj_pos, node_pos, time_setup, line_setup, true);
  }
}
