#pragma once

#include "MoMaUI.h"
#include "ofxGuiExtended.h"

using namespace MoMa;
using namespace std;

namespace rAIse {

/** @brief Organize an UI panel that allows to play live with drawing (time & line) parameters. */
class DrawingPanel : public Canvas {
 public:
  /**
   * @brief Construct a new Drawing Panel object.
   * 
   * @param app Pointer to a `MoMa::SceneApp` required when implementing Canvas
   * @param parent_ui Pointer to an other UI item used to place the panel
   * @param width Target width of the panel (auto-size on height)
   * @param position Position of the Panel relative to an other canvas
   * @param alignment Aligment of the panel on screen
   */
  DrawingPanel(SceneApp* app,
               Canvas* parent_ui = nullptr,
               const unsigned width = 325u,
               const Position position = Position::LEFT,
               const Position alignment = Position::LEFTSIDE);
};

}  // namespace rAIse