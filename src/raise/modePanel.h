#pragma once

#include "MoMaUI.h"
#include "ofMain.h"
#include "ofxGuiExtended.h"

using namespace MoMa;
namespace rAIse {

/** @brief Class that creates a subset of UI with dynamic constructor parameters. */
class ModePanel : public Canvas {
 public:
  /**
   * @brief Construct a new Mode Panel object.
   * 
   * @param app Pointer to a `MoMa::SceneApp` required when implementing Canvas
   * @param title Canvas displayed title
   * @param parameters  a group of parameter-equivalent UI component to create
   * @param styles A list of `ofJson` to be applied on each parameter of the group
   * @param parent_ui Pointer to an other UI item used to place the panel
   * @param position Position of the Panel relative to an other canvas
   * @param alignment Aligment of the panel on screen
   * @param parent_ui Pointer to an other UI item used to place the panel
   */
  ModePanel(SceneApp* app,
            const string& title,
            ofParameterGroup parameters,
            const vector<ofJson> styles,
            Canvas* parent_ui = nullptr,
            const unsigned width = 325u,
            const Position position = Position::DEFAULT,
            const Position alignment = Position::DEFAULT);

  /** @brief Getter / setter for the active property. */
  const bool active() const { return active_; }
  void set_active(const bool active) { active_ = active; }

  /** @brief Return the parameters' group controlled within this panel. */
  const ofParameterGroup& parameters() const { return parameters_; }

 private:
  ofParameter<bool> active_;
  ofParameterGroup parameters_;
};
}  // namespace rAIse