
#include "../raiseApp.h"

#include "drawingPanel.h"
#include "skeleton.h"

rAIse::DrawingPanel::DrawingPanel(SceneApp* app,
                                  Canvas* parent_ui,
                                  const unsigned width,
                                  const Position position,
                                  const Position alignment) : Canvas(app,
                                                                     "rAIse controls",
                                                                     Canvas::Type::Group,
                                                                     position,
                                                                     alignment,
                                                                     parent_ui) {
  _container->setConfig({{"width", width}});

  RaiseApp* raise_app = static_cast<RaiseApp*>(_app);

  const ofJson flex_style = {{"width", "100%"},
                             {"flex-direction", "row"},
                             {"flex-wrap", "wrap"},
                             {"margin", "2 2 10 2"}};
  const ofJson group_style = {{"margin", "5 6 15 6"}};
  const ofJson label_style = {{"width", "100%"}, {"background-color", "transparent"}};
  const ofJson slider_style = {{"width", "50%"}, {"precision", 2}};

  ofxGuiContainer* line_opt_container = _container->addContainer("line setup", flex_style);
  line_opt_container->addLabel("Line setup", label_style);

  ofParameter<int> line_iteration("iteration", rAIse::LINE_ITERATION, 1u, 15u);
  raise_app->line_setup.add(line_iteration);
  line_opt_container->add(raise_app->line_setup.getInt("iteration"), {{"width", "50%"}});

  ofParameter<int> line_resolution("resolution", rAIse::LINE_RESOLUTION, 0u, 100u);
  raise_app->line_setup.add(line_resolution);
  line_opt_container->add(raise_app->line_setup.getInt("resolution"), {{"width", "50%"}});

  ofParameter<float> line_twist("twist", 0.f, -M_PI * 4.f, M_PI * 4.f);
  raise_app->line_setup.add(line_twist);
  line_opt_container->add(raise_app->line_setup.getFloat("twist"), slider_style);

  ofParameter<int> line_radius("radius", 0u, 0u, rAIse::MAX_RADIUS);
  raise_app->line_setup.add(line_radius);
  line_opt_container->add(raise_app->line_setup.getInt("radius"), {{"width", "50%"}});

  ofParameter<int> line_noise("noise", 0u, 0u, rAIse::MAX_NOISE);
  raise_app->line_setup.add(line_noise);
  line_opt_container->add(raise_app->line_setup.getInt("noise"), {{"width", "100%"}});

  ofParameter<bool> line_follow("follow", false);
  raise_app->line_setup.add(line_follow);
  line_opt_container->add(raise_app->line_setup.getBool("follow"), {{"width", "50%"}});

  ofParameter<bool> line_ease("ease", false);
  raise_app->line_setup.add(line_ease);
  line_opt_container->add(raise_app->line_setup.getBool("ease"), {{"width", "50%"}});

  ofxGuiContainer* time_opt_container = _container->addContainer("time setup", flex_style);
  time_opt_container->addLabel("Time setup", label_style);

  ofParameter<float> time_speed("speed", rAIse::TIME_SPEED, 0.f, 1.f);
  raise_app->time_setup.add(time_speed);
  time_opt_container->add(raise_app->time_setup.getFloat("speed"), {{"width", "100%"}});

  ofParameter<float> time_phase("phase", .0f);
  raise_app->time_setup.add(time_phase);
  time_opt_container->add(raise_app->time_setup.getFloat("phase"), slider_style);

  ofParameter<float> time_shift("shift", .0f);
  raise_app->time_setup.add(time_shift);
  time_opt_container->add(raise_app->time_setup.getFloat("shift"), slider_style);

  ofParameter<bool> time_reverse("reverse", false);
  raise_app->time_setup.add(time_reverse);
  time_opt_container->add(raise_app->time_setup.getBool("reverse"), {{"width", "50%"}});

  ofParameter<bool> time_freeze("freeze", false);
  raise_app->time_setup.add(time_freeze);
  time_opt_container->add(raise_app->time_setup.getBool("freeze"), {{"width", "50%"}});

  ofxGuiContainer* trail_opt_container = _container->addContainer("trail setup", flex_style);
  trail_opt_container->addLabel("Trail setup", label_style);

  ofParameter<int> trail_amount("amount", 1u, 1u, rAIse::MAX_TRAIL);
  raise_app->trail_setup.add(trail_amount);
  trail_opt_container->add(raise_app->trail_setup.getInt("amount"), {{"width", "50%"}});

  ofParameter<int> trail_jump("jump", 1u, 1u, 10u);
  raise_app->trail_setup.add(trail_jump);
  trail_opt_container->add(raise_app->trail_setup.getInt("jump"), {{"width", "50%"}});

  initCanvas();
}