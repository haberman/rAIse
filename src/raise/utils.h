#pragma once

#include <armadillo>

#include "MoMa.h"
#include "ofMain.h"

using namespace MoMa;
using namespace std;

namespace rAIse {

/** @brief Contains static methods, mostly for computing gemoetries' transformations. */
class Utils {
 public:
  /**
   * @brief Project a vector along a normal.
   * 
   * @param point Point to project
   * @param normal Vector on which projection occures
   * 
   * @return glm::vec3 The resulting normalized projection.
   */
  static const glm::vec3 project_vector(const glm::vec3& point,
                                        const glm::vec3& normal) {
    const float sqrt_mag = glm::dot(normal, normal);
    if (sqrt_mag < FLT_EPSILON) {
      return glm::vec3();
    } else {
      return normal * glm::dot(point, normal) / sqrt_mag;
    }
  }

  /**
   * @brief Project a vector on a plane at a certain distance of a normal.
   * 
   * @param point Point to project
   * @param normal Normal (orthogonal) vector
   * @param distance Amount of projection
   * 
   * @return glm::vec3 The resulting projection.
   */
  static const glm::vec3 project_plane(const glm::vec3& point,
                                       const glm::vec3& normal,
                                       const glm::vec3& distance) {
    return point - project_vector(point + distance, normal);
  }

  /**
   * @brief Project a point on the great circle of a sphere.
   * 
   * @param origin The center of projection
   * @param quat How the great circle should be oriented in space
   * @param radius How far the projection
   * @param angle PI factor of the great circle
   * 
   * @return glm::vec3 The resulting projection.
   */
  static const glm::vec3 project_circle(const glm::vec3& origin,
                                        const glm::quat& quat,
                                        const float radius,
                                        const float angle) {
    const glm::vec3 oriented = quat * glm::vec3(1.f, 1.f, 1.f);
    return origin + glm::rotate(glm::vec3(1.f, 1.f, 1.f), angle, oriented) * radius;
  }

  /**
   * @brief Cartesian projection of a point being randomly projected on the surface of a sphere.
   * 
   * @param origin Point to project
   * @param radius Distance of projection
   * 
   * @return glm::vec3 The resulting projection.
   */
  static const glm::vec3 project_sphere(const glm::vec3& origin, const float radius) {
    const double zenith = arma::randu() * 2 * M_PI;
    const double azimuth = arma::randu() * 2 * M_PI;

    return project_sphere(origin, zenith, azimuth, radius);
  }

  /**
   * @brief Caresion projection of a point on the surface of a sphere being using euler angles.
   * 
   * @param origin Point to project
   * @param zenith X-axis angle
   * @param polar Z-axis angle
   * @param radius Length of projected ray
   */
  static const glm::vec3 project_sphere(const glm::vec3& origin,
                                        const float zenith,
                                        const float azimuth,
                                        const float radius) {
    glm::vec3 ret;

    ret.x = radius * cos(zenith) * sin(azimuth);
    ret.y = radius * sin(zenith) * sin(azimuth);
    ret.z = radius * cos(zenith);

    return ret + origin;
  }
};
}  // namespace rAIse