#pragma once

#include "MoMa.h"
#include "ofMain.h"

#include "node.h"

using namespace std;
using namespace MoMa;

namespace rAIse {

const int MIN_DISTANCE = -5000;      /*!< Maximum distance of plane projection from floor. */
const unsigned MAX_DISTANCE = 4000u; /*!< Maximum distance of plane projection from floor. */
const unsigned MAX_RADIUS = 50u;     /*!< Maximum amount of radius to apply on random or twist. */
const unsigned MAX_NOISE = 50u;      /*!< Maximum amount of noise to affect computed positions. */
const unsigned MAX_TRAIL = 20u;      /*!< Maximum amount of rAIse::Segment to append as trailing nodes. */

const static unsigned LINE_ITERATION = 5u;   /*!< Initial amount of segment's drawing iteration. */
const static unsigned LINE_RESOLUTION = 18u; /*!< Resolution of lines. */
const static float TIME_SPEED = .5f;         /*!< Initial speed of drawing animation. */

const static string MODE_RANDOM = "Random";            /*!< Draw nodes randomly. */
const static string MODE_BODY = "Full body";           /*!< Draw every nodes following the skeleton structure. */
const static string MODE_PROJECT = "Plane projection"; /*!< Draw nodes by projecting them onto a plane. */

/** @brief Object describing a skelton.
 * Basically, this consiststs of an abstraction for gathering nodes in a drawble fashion.
 */
class Skeleton {
 public:
  /**
   * @brief Construct a new Skeleton object
   * 
   * @param app `MoMa::SceneApp` pointer for which the track is involved.
   * @param track Pointer to the track being loaded by the `ofMotionMachine`
   * @param mode Initial raise::Mode
   */
  Skeleton(SceneApp* app,
           shared_ptr<Track> track,
           const string& mode);

  /** @brief Destroy the Skeleton object. */
  ~Skeleton();

  /**
   * @brief Get every child nodes starting at a given id.
   * 
   * @param nodes A map of nodes' references
   * @param q_id Where to start the query from
   * 
   * @return vector<int> A list of ids fitting the query.
   * @see rAIse::Node 
   */
  static vector<int> query_fw(map<int, shared_ptr<Node>> nodes, int q_id);

  /**
   * @brief Get every parent nodes starting at a given id.
   * 
   * @param nodes A map of nodes' references
   * @param q_id Where to start the query from
   * 
   * @return vector<int> A list of ids fitting the query.
   * @see rAIse::Node
   */
  static vector<int> query_rw(map<int, shared_ptr<Node>> nodes, int q_id);

  /**
   * @brief Get a random number of nodes
   * 
   * @param nodes A map of nodes' references
   * @param amount The amount of random nodes to stack
   * 
   * @return vector<int> A list of ids fitting the query.
   * @see rAIse::Node
   */
  static vector<int> query_random(map<int, shared_ptr<Node>> nodes, unsigned amount);

  /** @brief Update time phase for each active nodes.
   * 
   * @param value The phase value to be applied
   */
  void apply_timephase(const float& value);

  /** @brief Update tim shift for each active nodes.
   * 
   * @param value The amount of shfit to apply
   */
  void apply_timeshift(const float& value);

  /** @brief Setup the amount of trails for each active nodes.
   * 
   * @param value The amount of trails to draw
   */
  void apply_trailamount(const int& value);

  /** @brief Setup the amount of frames between trailing segments.
   * 
   * @param value The amount of frames to jump over befor storing a new trail segment.
   */
  void apply_trailjump(const int& value);

  /**
   * @brief Draw each segment for nodes whose ids are actives.
   * 
   * @param frame `MoMa::Frame` being actually displayed
   * @param now Frame index inside the loaded `MoMa::Track`
   * @param body_setup An `ofParameterGroup` specifically packed with random's parameters
   * @param time_setup An `ofParameterGroup` to hold time related options
   * @param line_setup An `ofParameterGroup` to hold line drawing styles
   * @param trail_setup `ofParameterGroup` to hold trailing parameters
   */
  void draw_body(const Frame* frame,
                 const int now,
                 const ofParameterGroup& body_setup,
                 const ofParameterGroup& time_setup,
                 const ofParameterGroup& line_setup,
                 const ofParameterGroup& trail_setup);

  /**
   * @brief Draws a random segment following the provided parameters
   * 
   * @param frame `MoMa::Frame` being actually displayed
   * @param now Frame index inside the loaded `MoMa::Track`
   * @param random_setup An `ofParameterGroup` specifically packed with random's parameters
   * @param mix Chance of a new set of random segments to be picked up
   * @param time_setup An `ofParameterGroup` to hold time related options
   * @param line_setup An `ofParameterGroup` to hold line drawing styles
   * @param trail_setup `ofParameterGroup` to hold trailing parameters
   */
  void draw_random(const Frame* frame,
                   const int now,
                   const ofParameterGroup& random_setup,
                   const ofParameterGroup& time_setup,
                   const ofParameterGroup& line_setup,
                   const ofParameterGroup& trail_setup);
  /**
   * @brief Draw each sekeleton node as a being projected onto a virtual plane.
   * 
   * @param frame MoMa::Frame being actually displayed
   * @param now Frame index inside the loaded MoMa::Track
   * @param project_setup An `ofParameterGroup` epecifically packed with projection's parameters
   * @param time_setup An `ofParameterGroup` to hold time related options
   * @param line_setup An `ofParameterGroup` to hold line drawing styles
   * @param trail_setup `ofParameterGroup` to hold trailing parameters
   */
  void draw_projection(const Frame* frame,
                       const int now,
                       const ofParameterGroup& project_setup,
                       const ofParameterGroup& time_setup,
                       const ofParameterGroup& line_setup,
                       const ofParameterGroup& trail_setup);

  /** @brief Clear nodes of provided mode. */
  void clean_mode(const string& mode) {
    if (!active_nodes_[mode].empty()) active_nodes_[mode].clear();
  }

  /**
   * @brief Clone the nodes_ reference list to active_nodes_.
   * 
   * @param mode The mode for which the operation is requested
   * @param trail_amount The amount of trails
   * @param trail_jump The amount of trails
   */
  void copy_nodes(const string& mode,
                  const unsigned trail_amount,
                  const unsigned trail_jump) {
    active_nodes_[mode].clear();

    for (unsigned i = 0; i < nodes_.size(); ++i) {
      shared_ptr<Node> new_node = make_shared<Node>(*nodes_[i]);
      new_node->set_trail_amount(trail_amount);
      new_node->set_trail_jump(trail_jump);

      active_nodes_[mode].emplace_back(new_node);
    }
  }

 private:
  shared_ptr<Track> track_;
  SceneApp* app_;

  map<int, shared_ptr<Node>> nodes_;                   /*!< Untampered list of every nodes. */
  map<string, vector<shared_ptr<Node>>> active_nodes_; /*!< Per mode list of nodes. */

  ofStyle joint_style_;
  ofStyle bone_style_;
};

}  // namespace rAIse