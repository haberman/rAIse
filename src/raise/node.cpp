#include "node.h"
#include "utils.h"

void rAIse::Node::draw(const glm::vec3& parent_pos,
                       const glm::vec3& node_pos,
                       const unique_ptr<glm::quat> bone_rot,
                       const float& scale,
                       const ofParameterGroup& time_setup,
                       const ofParameterGroup& line_setup) {
  glm::vec3 root, diff;
  if (time_setup.getBool("reverse")) {
    root = parent_pos;
    diff = node_pos - parent_pos;
  } else {
    root = node_pos;
    diff = parent_pos - node_pos;
  }

  draw_segments(trail_amount_ > 1 && ofGetFrameNum() % trail_jump_ == 0,
                root,
                diff,
                time_setup,
                line_setup,
                bone_rot.get());
}

void rAIse::Node::draw(const glm::vec3& parent_pos,
                       const glm::vec3& node_pos,
                       const ofParameterGroup& time_setup,
                       const ofParameterGroup& line_setup,
                       const bool use_projection) {
  glm::vec3 root, diff;
  if (time_setup.getBool("reverse")) {
    root = parent_pos;
    diff = node_pos - parent_pos;
  } else {
    root = node_pos;
    diff = parent_pos - node_pos;
  }

  draw_segments(ofGetFrameNum() % trail_jump_ == 0,
                root,
                diff,
                time_setup,
                line_setup);
}

void rAIse::Node::draw_segments(const bool append,
                                const glm::vec3& root,
                                const glm::vec3& diff,
                                const ofParameterGroup& time_setup,
                                const ofParameterGroup& line_setup,
                                const glm::quat* bone_rot) {
  if ((destroyable_ && !segments_.empty()) || segments_.size() > trail_amount_) {
    segments_.pop_back();
  }

  if (segments_.size() > 1) {
    ofSetColor(ofColor::whiteSmoke);
    for (unsigned i = 1; i < segments_.size(); ++i) {
      if (segments_[i].points.empty()) continue;

      for (unsigned j = 0; j < segments_[i].points.size() - 1; ++j) {
        const glm::vec3& p1 = segments_[i].points[j];
        const glm::vec3& p2 = segments_[i].points[j + 1];

        ofDrawLine(p1, p2);
      }
    }
  }

  Segment& front_segment = segments_.front();
  Segment segment;
  segment.phase = front_segment.phase;
  segment.shift = front_segment.shift;
  segment.progress = front_segment.progress + time_setup.getFloat("speed") * segment.phase;

  const float top = tan(segment.progress + segment.shift);

  if (top < -.25f || top > 1.25f) {  // only append points if line aperture is within bone
    front_segment.progress = segment.progress;
    in_ = false;

    return;
  }

  in_ = true;
  const float in = ofClamp(top - .25f, 0.f, 1.f);
  const float out = ofClamp(top + .25f, 0.f, 1.f);

  const bool freeze = time_setup.getBool("freeze");
  const bool follow = line_setup.getBool("follow");
  const float noise = arma::randu() * (float)line_setup.getInt("noise");
  float radius = line_setup.getInt("radius");

  const float res_progress = 1.f / (float)line_setup.getInt("resolution");
  const float twist_progress = 1.f / line_setup.getFloat("twist");
  const glm::vec3& p_vec = diff * res_progress;

  for (unsigned i = 0; i < line_setup.getInt("iteration"); ++i) {
    unsigned j = 0;
    for (float f = in; f < out; f += res_progress) {
      const glm::vec3 center = root + f * diff;
      const float angle = f * line_setup.getFloat("twist");

      if (line_setup.getBool("ease")) {
        radius *= sin(f * M_PI);
      }

      glm::vec3 p1, p2;

      if (follow && j > 0)  // starts drawing from previous node
      {
        p1 = segment.points[j];
      } else if (freeze) {  // @TODO this causes segfault
        const Segment& lower_segment = segments_.back();
        const vector<glm::vec3>& lower_points = lower_segment.points;

        if (j < lower_points.size()) {
          p1 = lower_points[j];
        } else {
          p1 = lower_segment.points[floor(lower_points.size() * arma::randu())];
        }

      } else if (bone_rot) {
        p1 = rAIse::Utils::project_circle(center, *bone_rot, radius, angle);
      } else {
        p1 = center;
      }

      if (noise > 0.f && !follow) {
        p1 = Utils::project_sphere(p1, noise);
      }

      if (bone_rot) {
        p2 = rAIse::Utils::project_circle(center + p_vec, *bone_rot, radius, angle);
      } else {
        p2 = center + p_vec;
      }

      if (noise > 0.f) {
        p2 = Utils::project_sphere(p2, noise);
      }

      if (j == 0) {
        segment.points.emplace_back(p1);
      }
      segment.points.emplace_back(p2);

      ofSetColor(ofColor::indianRed);
      ofDrawLine(p1, p2);

      ++j;
    }
  }
  // }
  if (append && !destroyable_) {
    segments_.emplace(segments_.begin(), segment);
  } else {
    front_segment.progress = segment.progress;
  }
}