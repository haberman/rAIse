# rAIse
## One human, a motion capture device and a computer are in the kinesphere, some code drops in...
This repo contains the source code of an ongoing art project which involves motion capture and kinethics analysis in a dance show about slowliness and inner speed.

### Dependencies
- [ofxMotionMachine](https://github.com/nadous/ofxMotionMachine)
- [ofxGuiExtended](https://github.com/nadous/ofxGuiExtended)

### Todo's
1. [x] group inputs and reduce `draw()` amount of parameters.
2. [x] fix drawing glitches.
3. [x] make trails.
4. [x] implements deviance in plane projection drawing;
- [ ] it should not be too costly to compute a dynamic speed base on distance between (which we know to be fixed in `FULL_BODY` mode, cost may comes in `RANDOM` & `PLAN_PROJECT` modes);
- [ ] fix crash on exit.
- [ ] make better lines;
- [ ] makes a styling panel for these ones;
- [ ] draw on scene too;
- [ ] implement NuiTrack live tracking;
- [ ] use masks;
- [ ] exposes ui input for midi hardware connectivity.

### History
- [21/06/2018] -> creates README.
- [22/06/2018] -> checked `1`.
- [02/07/2018] -> checked `2`, `3`.
- [05/07/2018] -> checked `4`.
